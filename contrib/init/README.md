Sample configuration files for:
```
SystemD: sssd.service
Upstart: sssd.conf
OpenRC:  sssd.openrc
         sssd.openrcconf
CentOS:  sssd.init
macOS:    org.sss.sssd.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
